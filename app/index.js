'use strict';
const Generator = require('yeoman-generator');
const fs = require('fs');
const camelCase = require('camel-case');

module.exports = class extends Generator {
    constructor(args, opts) {
        super(args, opts);
        this.log('Initializing...');

        this.addEntryToServiceLoader = function (serviceLoaderPath, answers) {
            var content = fs.readFileSync(serviceLoaderPath, 'utf8');

            var varName = camelCase( answers.controller ) + 'Service';
            var line1 = 'const ' + varName +' =  require(\'./Services/' + answers.controller + '\');';
            var line2 = varName + '.createAndUseService( sequelize, app);';
            var contentToAdd = line1 + '\r\n';
            contentToAdd += '\t' + line2 + '\r\n';
            contentToAdd += '\r\n';
            contentToAdd += '\t//[#ADDED_MODULES#]';
            
            var newContent = content.replace('//[#ADDED_MODULES#]', contentToAdd );

            fs.writeFileSync( serviceLoaderPath, newContent);
        }
    }

    start() {
        this.prompt([
            {
                type: 'input',
                name: 'controller',
                message: 'Enter a name for controller : ',
            },
            {
                type: 'input',
                name: 'endPoint',
                message: 'Enter a name for end point for controller : ',
            }
        ]).then((answers) => {
            // create destination folder

            //this.destinationRoot('Services');
            var from = this.templatePath('Services/Service.tmpl');
            var to = this.destinationPath('Services/' + answers.controller + '.js');
            this.log("From: " + from);
            this.log("To: " + to);
            this.fs.copyTpl(from, to,
                { controller: answers.controller, endPoint: answers.endPoint }
            );

            this.addEntryToServiceLoader(this.destinationPath('ServiceLoader.js'), answers);
        });
    }
}
